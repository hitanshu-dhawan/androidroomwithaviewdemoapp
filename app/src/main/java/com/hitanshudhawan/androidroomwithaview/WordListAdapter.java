package com.hitanshudhawan.androidroomwithaview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hitanshu on 15/6/18.
 */

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {

    private Context context;
    private OnItemClicked onItemClicked;
    private List<Word> words;

    public WordListAdapter(Context context, OnItemClicked onItemClicked) {
        this.context = context;
        this.onItemClicked = onItemClicked;
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WordViewHolder(LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,parent,false));
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        if (words != null) {
            Word current = words.get(position);
            holder.textView.setText(current.getId() + "    " + current.getWord());
        } else {
            // Covers the case of data not being ready yet.
            holder.textView.setText("No Word");
        }
    }

    void setWords(List<Word> words){
        this.words = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (words != null)
            return words.size();
        else return 0;
    }

    public class WordViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        private WordViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(android.R.id.text1);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClicked.onClick(view, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return onItemClicked.onLongClick(view, getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClicked {
        void onClick(View view, int position);
        boolean onLongClick(View view, int position);
    }

}
